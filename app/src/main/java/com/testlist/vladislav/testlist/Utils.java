package com.testlist.vladislav.testlist;

import android.util.Log;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Util class for working with json
 */
class Utils {
    private static final String TAG = Utils.class.getSimpleName();

    /**
     * Parse json string to list of comments in lowerBound and upperBound diapason
     *
     * @param json       - String for parse
     * @param lowerBound - lower id bound
     * @param upperBound - upper id bound
     * @return list of comments
     */
    static ArrayList<CommentEntity> getCommentList(String json, int lowerBound, int upperBound) {
        ArrayList<CommentEntity> result = new ArrayList<>();
        try {
            JSONArray jsonArray = new JSONArray(json);
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject object = jsonArray.getJSONObject(i);
                CommentEntity entity = new Gson().fromJson(object.toString(), CommentEntity.class);
                if (entity.getId() >= lowerBound && entity.getId() <= upperBound) {
                    result.add(entity);
                }
            }
        } catch (JSONException e) {
            Log.e(TAG, "getCommentList: ", e);
        }
        return result;
    }
}
