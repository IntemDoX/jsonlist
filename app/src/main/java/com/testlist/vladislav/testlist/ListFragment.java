package com.testlist.vladislav.testlist;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

/**
 * This fragment shows list of comments.
 */
public class ListFragment extends Fragment {
    private static final String KEY_LIST = "key_list";
    private static final int LIMIT = 10;
    private boolean loading;
    private int currentPosition;
    private RecyclerView recyclerView;
    private ListAdapter adapter;
    private LinearLayoutManager linearLayoutManager;
    private List<CommentEntity> fullCommentsList;
    private List<CommentEntity> paginationCommentsList;

    static ListFragment getInstance(ArrayList<CommentEntity> commentsList) {
        ListFragment fragment = new ListFragment();
        Bundle args = new Bundle();
        args.putParcelableArrayList(KEY_LIST, commentsList);
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_list, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        recyclerView = view.findViewById(R.id.recycler);
        initRecyclerView();
        if (getArguments() != null && getArguments().containsKey(KEY_LIST)) {
            fullCommentsList = getArguments().getParcelableArrayList(KEY_LIST);
            loadNewItems();
        }
    }

    /**
     * Initialize recyclerview
     */
    private void initRecyclerView() {
        paginationCommentsList = new ArrayList<>();
        fullCommentsList = new ArrayList<>();
        linearLayoutManager = new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false);
        recyclerView.setLayoutManager(linearLayoutManager);
        adapter = new ListAdapter(paginationCommentsList);
        recyclerView.setAdapter(adapter);
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                if (dy > 0) {
                    int visibleItemCount = linearLayoutManager.getChildCount();
                    int totalItemCount = linearLayoutManager.getItemCount();
                    int pastVisibleItems = linearLayoutManager.findFirstVisibleItemPosition();
                    if (!loading) {
                        if ((visibleItemCount + pastVisibleItems) >= totalItemCount) {
                            loading = true;
                            loadNewItems();
                            loading = false;
                        }
                    }
                }
            }
        });
    }

    /**
     * Loading new items when list is finished.
     */
    private void loadNewItems() {
        for (int i = currentPosition; i < currentPosition + LIMIT && i < fullCommentsList.size(); i++) {
            paginationCommentsList.add(fullCommentsList.get(i));
        }
        adapter.notifyItemRangeInserted(currentPosition, LIMIT);
        currentPosition += LIMIT;
    }
}
