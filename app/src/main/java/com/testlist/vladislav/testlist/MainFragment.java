package com.testlist.vladislav.testlist;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.PorterDuff;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputEditText;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

/**
 * Main fragment for input numbers
 */
public class MainFragment extends Fragment {
    private static final String TAG = MainFragment.class.getSimpleName();
    private int lowerBoundValue;
    private int upperBoundValue;
    private TextInputEditText lowerBoundEt;
    private OnFragmentChangedListener listener;
    private TextInputEditText upperBoundEt;
    private MaterialButton button;
    private ProgressBar progressBar;

    static MainFragment getInstance() {
        return new MainFragment();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof MainActivity) {
            listener = (OnFragmentChangedListener) context;
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_main, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        lowerBoundEt = view.findViewById(R.id.lower_bound_et);
        upperBoundEt = view.findViewById(R.id.upper_bound_et);
        progressBar = view.findViewById(R.id.progressBar);
        lowerBoundEt.getBackground().setColorFilter(getResources().getColor(R.color.red_500_primary), PorterDuff.Mode.SRC_ATOP);
        upperBoundEt.getBackground().setColorFilter(getResources().getColor(R.color.red_500_primary), PorterDuff.Mode.SRC_ATOP);
        button = view.findViewById(R.id.button);
        button.setOnClickListener(v -> btnAction());
        lowerBoundEt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() < 1) {
                    button.setEnabled(false);
                } else if (!isNumeric(s.toString())) {
                    lowerBoundEt.setError(getString(R.string.number_format_error_message));
                    button.setEnabled(false);
                } else {
                    lowerBoundEt.setError(null);
                    button.setEnabled(lowerBoundEt.getText() != null && upperBoundEt.getText() != null &&
                            lowerBoundEt.getText().length() > 0 && isNumeric(lowerBoundEt.getText().toString()) &&
                            upperBoundEt.getText().length() > 0 && isNumeric(upperBoundEt.getText().toString()));
                }
            }
        });
        upperBoundEt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() < 1) {
                    button.setEnabled(false);
                } else if (!isNumeric(s.toString())) {
                    upperBoundEt.setError(getString(R.string.number_format_error_message));
                    button.setEnabled(false);
                } else {
                    upperBoundEt.setError(null);
                    button.setEnabled(lowerBoundEt.getText() != null && upperBoundEt.getText() != null &&
                            lowerBoundEt.getText().length() > 0 && isNumeric(lowerBoundEt.getText().toString()) &&
                            upperBoundEt.getText().length() > 0 && isNumeric(upperBoundEt.getText().toString()));
                }
            }
        });
    }

    /**
     * Action by clicking on the button
     */
    private void btnAction() {
        button.setEnabled(false);
        lowerBoundValue = Integer.valueOf(lowerBoundEt.getText().toString());
        upperBoundValue = Integer.valueOf(upperBoundEt.getText().toString());
        new GetMethodDemo().execute();
    }

    /**
     * Async request to get json string of comments
     */
    @SuppressLint("StaticFieldLeak")
    public class GetMethodDemo extends AsyncTask<String, Void, String> {
        String serverResponse;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar.setVisibility(View.VISIBLE);
        }

        @Override
        protected String doInBackground(String... strings) {
            URL url;
            HttpURLConnection urlConnection;
            try {
                url = new URL("http://jsonplaceholder.typicode.com/comments");
                urlConnection = (HttpURLConnection) url.openConnection();
                int responseCode = urlConnection.getResponseCode();
                if (responseCode == HttpURLConnection.HTTP_OK) {
                    serverResponse = readStream(urlConnection.getInputStream());
                }
            } catch (IOException e) {
                Log.e(TAG, "doInBackground: ", e);
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progressBar.setVisibility(View.GONE);
            if (listener != null) {
                listener.openFragment(ListFragment.getInstance(Utils.getCommentList(serverResponse, lowerBoundValue, upperBoundValue)));
            }
            button.setEnabled(true);
        }
    }

    /**
     * Convert input bytes to String
     *
     * @param in - input stream
     * @return json string
     */
    private String readStream(InputStream in) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(in));
        StringBuilder response = new StringBuilder();
        try {
            String line;
            while ((line = reader.readLine()) != null) {
                response.append(line);
            }
        } catch (IOException e) {
            Log.e(TAG, "readStream: ", e);
        } finally {
            try {
                reader.close();
            } catch (IOException e) {
                Log.e(TAG, "readStream: ", e);
            }
        }
        return response.toString();
    }

    /**
     * Check is a string is a number
     *
     * @param str - string, which will be checked
     * @return true if string is number
     */
    private boolean isNumeric(String str) {
        return str.matches("-?\\d+(\\.\\d+)?");
    }

    /**
     * Interface for communication between activity and fragment
     */
    public interface OnFragmentChangedListener {
        /**
         * Open new fragment through activity
         *
         * @param fragment fragment than must be opened
         */
        void openFragment(Fragment fragment);
    }
}
