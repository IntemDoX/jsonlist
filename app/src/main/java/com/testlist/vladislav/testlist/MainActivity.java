package com.testlist.vladislav.testlist;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.FrameLayout;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

/**
 * Main activity. First and main screen
 */
public class MainActivity extends AppCompatActivity implements MainFragment.OnFragmentChangedListener {
    private FrameLayout container;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        container = findViewById(R.id.container);
        initialFragmentOpen();
    }

    /**
     * Add first fragment to activity
     */
    private void initialFragmentOpen() {
        getSupportFragmentManager().beginTransaction()
                .setCustomAnimations(R.anim.anim_fade_in, R.anim.anim_fade_out, R.anim.anim_fade_in, R.anim.anim_fade_out)
                .add(container.getId(), MainFragment.getInstance())
                .commit();
    }

    @Override
    public void openFragment(Fragment fragment) {
        hideKeyboard();
        getSupportFragmentManager().beginTransaction()
                .setCustomAnimations(R.anim.anim_fade_in, R.anim.anim_fade_out, R.anim.anim_fade_in, R.anim.anim_fade_out)
                .add(container.getId(), fragment)
                .addToBackStack(fragment.getTag())
                .commit();
    }

    /**
     * Hide keyboard functional.
     */
    private void hideKeyboard() {
        InputMethodManager imm = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
        View view = getCurrentFocus();
        if (view == null) {
            view = new View(this);
        }
        if (imm != null) {
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }
}
